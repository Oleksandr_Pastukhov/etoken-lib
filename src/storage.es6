import AccountStorage from 'account-storage';
import web3 from './web3';

var storage = new AccountStorage('0x152c21d6944f32c6b45605af12bb9b7231a456e7', web3);

export default storage;